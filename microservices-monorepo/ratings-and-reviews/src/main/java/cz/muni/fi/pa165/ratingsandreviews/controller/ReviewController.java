package cz.muni.fi.pa165.ratingsandreviews.controller;

import cz.muni.fi.pa165.ratingsandreviews.dto.ReviewDTO;
import cz.muni.fi.pa165.ratingsandreviews.facade.ReviewFacade;
import cz.muni.fi.pa165.ratingsandreviews.model.FilmProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller for handling review-related operations.
 */
@RestController
@RequestMapping(path = "/api/reviews", produces = MediaType.APPLICATION_JSON_VALUE)
public class ReviewController {
    /**
     * The ReviewFacade instance.
     */
    private final ReviewFacade reviewFacade;

    /**
     * Constructs a ReviewController with the given ReviewFacade.
     *
     * @param facade The ReviewFacade to be used.
     */
    public ReviewController(final ReviewFacade facade) {
        this.reviewFacade = facade;
    }

    /**
     * Adds a new review.
     *
     * @param newReview The review to be added.
     * @return The added review DTO.
     */
    @PostMapping()
    public ResponseEntity<ReviewDTO> addReview(@RequestBody final ReviewDTO newReview) {
        ReviewDTO addedReview = reviewFacade.addReview(newReview);
        return ResponseEntity.status(HttpStatus.CREATED).body(addedReview);
    }

    /**
     * Deletes a review by its ID.
     *
     * @param reviewId The ID of the review to be deleted.
     * @return The deleted review DTO.
     */
    @DeleteMapping("/{reviewId}")
    public ResponseEntity<ReviewDTO> deleteReview(@PathVariable final Long reviewId) {
        ReviewDTO deletedReview = reviewFacade.deleteReview(reviewId);
        return ResponseEntity.ok(deletedReview);
    }

    /**
     * Updates a review.
     *
     * @param updatedReview The updated review.
     * @return The updated review DTO.
     */
    @PutMapping("/{reviewId}")
    public ResponseEntity<ReviewDTO> updateReview(@RequestBody final ReviewDTO updatedReview) {
        ReviewDTO updated = reviewFacade.updateReview(updatedReview);
        return ResponseEntity.ok(updated);
    }

    /**
     * Retrieves a review by its ID.
     *
     * @param reviewId The ID of the review to be retrieved.
     * @return The retrieved review DTO.
     */
    @GetMapping("/{reviewId}")
    public ResponseEntity<ReviewDTO> getReview(@PathVariable final Long reviewId) {
        ReviewDTO review = reviewFacade.getReview(reviewId);
        return ResponseEntity.ok(review);
    }

    /**
     * Retrieves reviews by user ID.
     *
     * @param userId The ID of the user whose reviews are to be retrieved.
     * @return List of review DTOs.
     */
    @GetMapping("/getByUserId")
    public ResponseEntity<List<ReviewDTO>> getReviewsByUserId(@RequestParam final Long userId) {
        List<ReviewDTO> reviews = reviewFacade.getReviewsByUserId(userId);
        return ResponseEntity.ok(reviews);
    }

    /**
     * Retrieves all reviews sorted by a specific film property in chosen order.
     *
     * @param property The film property to sort by.
     * @param order order -> ASC/DESC
     * @return List of review DTOs.
     */
    @GetMapping("/sortedByProperty/{property}/{order}")
    public ResponseEntity<List<ReviewDTO>> getAllReviewsSortedByProperty(
            @PathVariable("property") final FilmProperty property,
            @PathVariable("order") final String order) {
        boolean descending = "desc".equalsIgnoreCase(order);
        List<ReviewDTO> reviews = reviewFacade.getAllReviewsSortedByProperty(property, descending);
        return ResponseEntity.ok(reviews);
    }

    /**
     * Retrieves all reviews sorted by overall rating of film.
     *
     * @param order order -> ASC/DESC
     * @return List of review DTOs.
     */
    @GetMapping("/sortedByRating/{order}")
    public ResponseEntity<List<ReviewDTO>> getAllReviewssSortedByProperty(
            @PathVariable("order") final String order) {
        boolean descending = "desc".equalsIgnoreCase(order);
        List<ReviewDTO> reviews = reviewFacade.getAllReviewsSortedByRating(descending);
        return ResponseEntity.ok(reviews);
    }
}
