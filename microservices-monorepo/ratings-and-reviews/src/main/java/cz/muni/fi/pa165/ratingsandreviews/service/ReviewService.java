package cz.muni.fi.pa165.ratingsandreviews.service;

import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service interface for managing reviews.
 */
@Service
@Primary
public interface ReviewService {

    /**
     * Adds a new review.
     *
     * @param newReview The review to be added.
     * @return The added review.
     */
    @Transactional
    Review addReview(Review newReview);

    /**
     * Deletes a review by its ID.
     *
     * @param reviewId The ID of the review to be deleted.
     * @return The deleted review.
     */
    @Transactional
    Review deleteReview(Long reviewId);

    /**
     * Updates an existing review.
     *
     * @param updatedReview The updated review.
     * @return The updated review.
     */
    @Transactional
    Review updateReview(Review updatedReview);

    /**
     * Retrieves a review by its ID.
     *
     * @param reviewId The ID of the review to be retrieved.
     * @return The retrieved review.
     */
    @Transactional
    Review getReview(Long reviewId);

    /**
     * Retrieves all reviews.
     *
     * @return List of all reviews.
     */
    @Transactional
    List<Review> getAllReviews();
}
