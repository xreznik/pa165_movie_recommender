package cz.muni.fi.pa165.ratingsandreviews.model;

/**
 * Enum representing different properties of a film that can be reviewed.
 */
public enum FilmProperty {
    /**
     * The plot of the film.
     */
    PLOT,

    /**
     * The acting in the film.
     */
    ACTING,

    /**
     * The sound design of the film.
     */
    SOUND,

    /**
     * The special effects in the film.
     */
    SPECIAL_EFFECTS,

    /**
     * The costumes in the film.
     */
    COSTUMES,
}
