package cz.muni.fi.pa165.ratingsandreviews.repository;

import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {
}

