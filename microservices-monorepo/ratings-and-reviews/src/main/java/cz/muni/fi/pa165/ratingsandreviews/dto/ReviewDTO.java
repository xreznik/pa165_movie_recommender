package cz.muni.fi.pa165.ratingsandreviews.dto;

import cz.muni.fi.pa165.ratingsandreviews.model.FilmProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * Data Transfer Object for representing a review.
 */
@Setter
@Getter
@Schema(title = "ReviewDTO", description = "Represents a standard review DTO object")
public class ReviewDTO {

    /**
     * -- SETTER --
     *  Sets the ID of the movie to which the review relates.
     * <p>
     * -- GETTER --
     *  Retrieves the ID of the movie to which the review relates.
     *

     */
    @Schema(description = "ID of the movie to which the review relates", example = "1234")
    private Long movieId;

    /**
     * -- GETTER --
     *  Retrieves the ID of the user who created the review.
     * <p>
     *
     * -- SETTER --
     *  Sets the ID of the user who created the review.
     *
     */
    @Schema(description = "ID of the user who created the review", example = "5678")
    private Long userId;

    /**
     * -- SETTER --
     *  Sets the ID of the review.
     * <p>
     * -- GETTER --
     *  Retrieves the ID of the review.
     *

     */
    @Schema(description = "ID of the review", example = "9876")
    private Long id;

    /**
     * -- GETTER --
     *  Retrieves the ratings for different aspects of the movie.
     * <p>
     *
     * -- SETTER --
     *  Sets the ratings for different aspects of the movie.
     *
     */
    @Schema(description = "Ratings for different aspects of the movie")
    private Map<FilmProperty, Float> ratings;

    /**
     * -- GETTER --
     *  Retrieves the overall rating of the movie.
     * <p>
     *
     * -- SETTER --
     *  Sets the overall rating of the movie.
     *
     */
    @Schema(description = "Overall rating of the movie", example = "8.5")
    private Double overallRating;

}
