package cz.muni.fi.pa165.ratingsandreviews;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MovieRecommenderRatingsAndReviewsApplicationTests {

	@Test
	void contextLoads() {
		// Intentionally empty
	}

}
