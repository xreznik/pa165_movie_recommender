package cz.muni.fi.pa165.usermanagement.api;

import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;


@Schema(title = "UserDTO",
    description = "represents a standard user DTO object"
)
public record UserDTO(
    @NotBlank
    @Schema(description = "id", example = "5445646510544")
    long id,
    @NotBlank
    @Schema(description = "username", example = "user123456")
    String userName,
    @NotBlank
    @Schema(description = "email", example = "user123456@gmail.com")
    String email,

    @NotBlank
    @Schema(description = "user type", example = "USER")
    UserType userType
) {
}
