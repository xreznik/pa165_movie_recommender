package cz.muni.fi.pa165.usermanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point for the User Management Spring Boot application.
 * This class launches the Spring Boot application.
 */
@SuppressWarnings({"PMD", "checkstyle:hideutilityclassconstructor"})  // spring triggers linters false-positives
@SpringBootApplication
public class UserManagementApplication {

    /**
     * Main method to run the Spring Boot application.
     *
     * @param args the command line arguments passed to the application.
     */
    public static void main(final String[] args) {
        SpringApplication.run(UserManagementApplication.class, args);
    }

}
