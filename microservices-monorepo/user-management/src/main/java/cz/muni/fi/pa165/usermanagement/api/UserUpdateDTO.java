package cz.muni.fi.pa165.usermanagement.api;

import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(title = "User Update DTO",
    description = "Represents the data transfer object for user update operations")
public record UserUpdateDTO(
    @NotBlank
    @Schema(description = "username", example = "user123456")
    String userName,

    @NotBlank
    @Schema(description = "email", example = "user123456@gmail.com")
    String email,

    @Schema(description = "encrypted password", example = "****************",
        requiredMode = Schema.RequiredMode.NOT_REQUIRED)
    String encryptedPassword,

    @NotBlank
    @Schema(description = "user type", example = "USER")
    UserType userType
) {
}
