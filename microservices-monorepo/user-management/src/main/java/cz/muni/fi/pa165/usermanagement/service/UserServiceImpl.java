package cz.muni.fi.pa165.usermanagement.service;

import cz.muni.fi.pa165.usermanagement.data.model.User;
import cz.muni.fi.pa165.usermanagement.data.repository.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Primary
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    /**
     * Constructor for UserServiceImpl that takes a UserRepository.
     *
     * @param userRepository repository handling user data operations.
     */
    @Autowired
    public UserServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Fetches all users from the database.
     *
     * @return a List of User objects.
     */
    @Override
    @Transactional(readOnly = true)
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    /**
     * Saves a new user to the database.
     *
     * @param user the User object to be saved.
     * @return the saved User object.
     */
    @Override
    @Transactional
    public User createUser(final User user) {
        return userRepository.save(user);
    }

    /**
     * Retrieves a user by their ID.
     *
     * @param id the ID of the user to find.
     * @return the found User object.
     * @throws EntityNotFoundException if no user with the given ID exists.
     */
    @Override
    @Transactional(readOnly = true)
    public User getUser(final long id) {
        return userRepository.findById(id)
            .orElseThrow(() -> new EntityNotFoundException("User with id " + id + " does not exist"));
    }

    /**
     * Updates an existing user in the database.
     *
     * @param updatedUser the User object to update.
     */
    @Override
    @Transactional
    public void updateUser(final User updatedUser) {
        userRepository.save(updatedUser);
    }

    /**
     * Deletes a user from the database by their ID.
     *
     * @param id the ID of the user to delete.
     */
    @Override
    @Transactional
    public void deleteUser(final long id) {
        userRepository.deleteById(id);
    }
}
