package cz.muni.fi.pa165.usermanagement.rest;

import cz.muni.fi.pa165.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.usermanagement.api.UserRegistrationDTO;
import cz.muni.fi.pa165.usermanagement.api.UserUpdateDTO;
import cz.muni.fi.pa165.usermanagement.facade.UserFacade;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@OpenAPIDefinition(
    info = @Info(title = "User Management And Authentication",
        version = "0.1.0",
        description = """
            Microservice for registering and logging in users as well as their authentication.
            The API has operations for:
            - creating a user account
            - updating user account information
            - deleting a user account
            - getting a specific user account by its username
            - getting all the registered users
            """,
        contact = @Contact(name = "Matej Vavro", email = "536408@mail.muni.cz"),
        license = @License(name = "Apache 2.0", url = "https://www.apache.org/licenses/LICENSE-2.0.html")
    ),
    servers = @Server(description = "my server", url = "{scheme}://{server}:{port}", variables = {
        @ServerVariable(name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
        @ServerVariable(name = "server", defaultValue = "localhost"),
        @ServerVariable(name = "port", defaultValue = "8080"),
    })
)
@Tag(name = "User management and authentication",
    description = "Microservice for user management CRUD and authentication")
@RequestMapping(path = "/api/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserRestController {

    private final UserFacade userFacade;

    /**
     * Constructs a UserRestController with the specified UserFacade.
     *
     * @param userFacade the UserFacade to use for user-related operations.
     */
    @Autowired
    public UserRestController(final UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    /**
     * Handles the request to get all registered users.
     *
     * @return a ResponseEntity containing a list of UserDTOs.
     */
    @Operation(summary = "Returns all registered users", description = "Returns a list of all the registered users")
    @GetMapping(path = "")
    @CrossOrigin(origins = "*")
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        List<UserDTO> users = userFacade.getAllUsers();
        return ResponseEntity.ok(users);
    }

    /**
     * Handles the request to create a new user.
     *
     * @param user the UserRegistrationDTO containing the user details.
     * @return a ResponseEntity containing the created UserDTO.
     */
    @Operation(summary = "Creates a user",
        description = "Creates a user with the values that "
            + "are specified in the payload")
    @PostMapping(path = "")
    @CrossOrigin(origins = "*")
    public ResponseEntity<UserDTO> createUser(@Valid @RequestBody final UserRegistrationDTO user) {
        UserDTO createdUser = userFacade.createUser(user);
        return ResponseEntity.ok(createdUser);
    }

    /**
     * Handles the request to get a specific user by their ID.
     *
     * @param id the ID of the user to retrieve.
     * @return a ResponseEntity containing the found UserDTO or not found status.
     */
    @Operation(summary = "Returns the specified user",
        description = "Returns a user with the specified id if "
            + "he has been registered already")
    @GetMapping(path = "/{id}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<UserDTO> getUser(@PathVariable final Long id) {
        try {
            UserDTO foundUser = userFacade.getUser(id);
            return ResponseEntity.ok(foundUser);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Handles the request to update a specific user's details.
     *
     * @param id          the ID of the user to update.
     * @param userDetails the UserUpdateDTO containing the new user details.
     * @return a ResponseEntity with no content or not found status.
     */
    @Operation(summary = "Updates information about a user",
        description = "Updates information about a user by changing his attributes "
            + "with the ones specified in the payload")
    @PutMapping(path = "/{id}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<UserDTO> updateUser(@PathVariable final Long id,
                                              @Valid @RequestBody final UserUpdateDTO userDetails) {
        try {
            userFacade.updateUser(id, userDetails);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.noContent().build();
    }

    /**
     * Handles the request to delete a specific user by their ID.
     *
     * @param id the ID of the user to delete.
     * @return a ResponseEntity with no content.
     */
    @Operation(summary = "Deletes a user", description = "Deletes a user with the specified id")
    @DeleteMapping(path = "/{id}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<Void> deleteUser(@PathVariable final long id) {
        userFacade.deleteUser(id);
        return ResponseEntity.noContent().build();
    }
}
