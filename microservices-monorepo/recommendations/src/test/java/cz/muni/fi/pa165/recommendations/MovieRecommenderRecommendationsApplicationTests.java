package cz.muni.fi.pa165.recommendations;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MovieRecommenderRecommendationsApplicationTests {

    @Test
    void contextLoads() {
        // Intentionally empty ..
    }

}
