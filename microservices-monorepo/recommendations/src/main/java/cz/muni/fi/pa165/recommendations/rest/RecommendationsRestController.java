package cz.muni.fi.pa165.recommendations.rest;

import cz.muni.fi.pa165.recommendations.api.MovieDTO;
import cz.muni.fi.pa165.recommendations.facade.RecommendationsFacade;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@OpenAPIDefinition(
		info = @Info(title = "Movie Recommender - Recommendations",
				version = "0.1.0",
				description = "Recommendations microservice for the Movie Recommender Web application",
				contact = @Contact(name = "Aleksandr Verevkin", email = "555098@mail.muni.cz"),
				license = @License(
						name = "Apache 2.0",
						url = "https://www.apache.org/licenses/LICENSE-2.0.html"
				)
		),
		servers = @Server(description = "my server", url = "{scheme}://{server}:{port}", variables = {
				@ServerVariable(
						name = "scheme",
						allowableValues = {"http", "https"}, defaultValue = "http"
				),
				@ServerVariable(name = "server", defaultValue = "localhost"),
				@ServerVariable(name = "port", defaultValue = "8080"),
		})
)
@Tag(name = "Recommendations", description = "Recommendations microservice for the Movie Recommender Web application")
@RequestMapping("/api/recommendations")
@RestController
public class RecommendationsRestController {

	private final RecommendationsFacade recommendationsFacade;

	/**
	 * RecommendationsRestController constructor
	 *
	 * @param recommendationsFacade recommendations facade module
	 */
	@Autowired
	public RecommendationsRestController(final RecommendationsFacade recommendationsFacade) {
		this.recommendationsFacade = recommendationsFacade;
	}

	/**
	 * Returns 10 recommended movies for the given movie
	 *
	 * @param id ID of the movie
	 * @return list of recommended movies
	 */
	@Operation(
			summary = "Returns 10 recommended movies for the given movie",
			description = "Create and return list of 10 recommended movies for the given movie"
	)
	@GetMapping("/{id}")
	@CrossOrigin("*")
	public ResponseEntity<List<MovieDTO>> getRecommendedMovies(@PathVariable final Long id) {
		List<MovieDTO> recommendedMovies = recommendationsFacade.getRecommendedMovies(id);
		return ResponseEntity.ok(recommendedMovies);
	}

	/**
	 * Returns parametrized number of recommended movies for the given movie
	 *
	 * @param id ID of the movie
	 * @param count number of recommended movies to return
	 * @return list of recommended movies
	 */
	@Operation(
			summary = "Returns parametrized number of recommended movies for the given movie",
			description = "Create and return parametrized number of recommended movies for the given movie"
	)
	@GetMapping(value = "/{id}", params = {"count"})
	@CrossOrigin("*")
	public ResponseEntity<List<MovieDTO>> getRecommendedMovies(
			@PathVariable final Long id, @RequestParam final Integer count
	) {
		List<MovieDTO> recommendedMovies = recommendationsFacade.getRecommendedMovies(id, count);
		return ResponseEntity.ok(recommendedMovies);
	}
}
