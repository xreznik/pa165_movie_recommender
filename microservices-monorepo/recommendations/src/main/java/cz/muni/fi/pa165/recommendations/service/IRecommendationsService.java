package cz.muni.fi.pa165.recommendations.service;

import cz.muni.fi.pa165.recommendations.data.model.Movie;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IRecommendationsService {

	/**
	 * Get 10 recommended movies for a given movie
	 *
	 * @param movieId ID of the movie
	 * @return List of recommended movies
	 */
	List<Movie> getRecommendedMovies(Long movieId);

	/**
	 * Get parametrized number of recommended movies for a given movie
	 *
	 * @param movieId ID of the movie
	 * @param count number of recommended movies to return
	 * @return List of recommended movies
	 */
	List<Movie> getRecommendedMovies(Long movieId, int count);
}
