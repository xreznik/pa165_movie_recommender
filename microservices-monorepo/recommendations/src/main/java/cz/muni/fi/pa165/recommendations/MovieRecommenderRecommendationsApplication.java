package cz.muni.fi.pa165.recommendations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SuppressWarnings({"PMD", "checkstyle:hideutilityclassconstructor"})  // spring triggers linters false-positives
@SpringBootApplication
public class MovieRecommenderRecommendationsApplication {

	/**
	 * Main method to run the application.
	 *
	 * @param args command line arguments
	 */
	public static void main(final String[] args) {
		SpringApplication.run(MovieRecommenderRecommendationsApplication.class, args);
	}

}
