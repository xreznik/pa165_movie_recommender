package cz.muni.fi.pa165.recommendations.data.model;

import cz.muni.fi.pa165.recommendations.data.enums.MovieGenre;

import java.util.List;

import lombok.Getter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Movie {
	private Long id;
	private String title;
	private Integer year;
	private String director;
	private List<MovieGenre> genres;
	private List<String> actors;
	private Double rating;
}
