package cz.muni.fi.pa165.moviesmanagement.rest;

import cz.muni.fi.pa165.moviesmanagement.dto.CreateMovieDto;
import cz.muni.fi.pa165.moviesmanagement.dto.MovieDto;
import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.facade.MovieFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;


import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/api/movies")
@Tag(name = "Movies", description = "Endpoints for movies management")
public class MovieController {

    private final MovieFacade movieFacade;

    /**
     * Constructor for MovieController.
     *
     * @param movieFacade The movie facade.
     */
    @Autowired
    public MovieController(final MovieFacade movieFacade) {
        this.movieFacade = movieFacade;
    }

    /**
     * Get all movies
     *
     * @return collection of all movies
     */
    @GetMapping
    public ResponseEntity<Collection<MovieDto>> getAllMovies() {
        return ResponseEntity.ok(movieFacade.getAllMovies());
    }

    /**
     * Get movie by id
     *
     * @param id of movie
     * @return movie with given id
     */
    @GetMapping("/{id}")
    public ResponseEntity<MovieDto> getMovieById(@PathVariable final Long id) {
        Optional<MovieDto> movieDto = movieFacade.getMovieById(id);
        return movieDto.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Get movie by name
     *
     * @param name of movie
     * @return movie with given name
     */
    @Operation(summary = "Get movie by name", description = "Get movie by name", tags = {"Movies"})
    @GetMapping("/name/{name}")
    public ResponseEntity<MovieDto> getMovieByName(@PathVariable final String name) {
        Optional<MovieDto> movieDto = movieFacade.getMovieByName(name);
        return movieDto.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Get movies by genre
     *
     * @param genre of movie
     * @return movies with given genre
     */
    @GetMapping("/genre/{genre}")
    public ResponseEntity<Collection<MovieDto>> getMoviesByGenre(@PathVariable final MovieGenre genre) {
        return ResponseEntity.ok(movieFacade.getMoviesByGenre(genre));
    }

    /**
     * Get all possible genres
     *
     * @return collection of all possible genres
     */
    @GetMapping("/genres")
    public ResponseEntity<Collection<MovieGenre>> getGenres() {
        return ResponseEntity.ok(movieFacade.getPossibleGenres());
    }
    /**
     * Add movie
     *
     * @param movieDto to be added
     * @return added movie
     */
    @PostMapping
    public ResponseEntity<MovieDto> addMovie(@RequestBody final CreateMovieDto movieDto) {
        MovieDto createdMovie = movieFacade.addMovie(movieDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdMovie);
    }

    /**
     * Update movie
     *
     * @param movieDto to be updated
     * @return updated movie
     */
    @PutMapping
    public ResponseEntity<MovieDto> updateMovie(@RequestBody final MovieDto movieDto) {
        Optional<MovieDto> updatedMovie = Optional.ofNullable(movieFacade.updateMovie(movieDto));
        return updatedMovie.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Delete movie
     *
     * @param id of movie to be deleted
     * @return true if movie was deleted, false otherwise
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteMovie(@PathVariable final Long id) {
        var deleted = movieFacade.deleteMovie(id);
        return deleted ? ResponseEntity.ok(true) : ResponseEntity.notFound().build();
    }
}
