package cz.muni.fi.pa165.moviesmanagement.facade;

import cz.muni.fi.pa165.moviesmanagement.dto.CreateMovieDto;
import cz.muni.fi.pa165.moviesmanagement.dto.MovieDto;
import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;

import java.util.Collection;
import java.util.Optional;

public interface MovieFacade {
    /**
     * Get all movies
     *
     * @return collection of all movies
     */
    Collection<MovieDto> getAllMovies();

    /**
     * Retrieves a movie by its identifier.
     *
     * @param id The identifier of the movie.
     * @return An Optional containing the movie DTO if found, or an empty Optional otherwise.
     */
    Optional<MovieDto> getMovieById(Long id);

    /**
     * Retrieves a movie by its name.
     *
     * @param name The name of the movie.
     * @return An Optional containing the movie DTO if found, or an empty Optional otherwise.
     */

    Optional<MovieDto> getMovieByName(String name);

    /**
     * Retrieves all movies of a given genre.
     *
     * @param genre The genre to filter the movies.
     * @return A collection of movie DTOs of the specified genre.
     */
    Collection<MovieDto> getMoviesByGenre(MovieGenre genre);

    /**
     * Retrieves all possible movie genres.
     *
     * @return A collection of all possible movie genres.
     */
    Collection<MovieGenre> getPossibleGenres();

    /**
     * Adds a new movie to the repository.
     *
     * @param movieDto The movie DTO to be added.
     * @return The added movie DTO.
     */
    MovieDto addMovie(CreateMovieDto movieDto);

    /**
     * Deletes a movie by its identifier.
     *
     * @param id The identifier of the movie to be deleted.
     * @return true if the movie was successfully deleted, false otherwise.
     */
    Boolean deleteMovie(Long id);

    /**
     * Updates an existing movie.
     *
     * @param movieDto The movie DTO with updated fields.
     * @return The updated movie DTO.
     */
    MovieDto updateMovie(MovieDto movieDto);
}
