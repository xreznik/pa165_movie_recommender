package cz.muni.fi.pa165.moviesmanagement.service;

import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.model.Movie;
import cz.muni.fi.pa165.moviesmanagement.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class MovieService {

    private final MovieRepository movieRepository;

    /**
     * Constructor for MovieService.
     *
     * @param movieRepository The movie repository.
     */
    @Autowired
    public MovieService(final MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    /**
     * Retrieves all movies.
     *
     * @return A collection of all movies.
     */
    public Collection<Movie> getAll() {
        return movieRepository.getAll();
    }

    /**
     * Retrieves a movie by its identifier.
     *
     * @param id The identifier of the movie.
     * @return An Optional containing the movie if found, or an empty Optional otherwise.
     */
    public Optional<Movie> findById(final Long id) {
        return movieRepository.findById(id);
    }

    /**
     * Retrieves a movie by its name.
     *
     * @param name The name of the movie.
     * @return An Optional containing the movie if found, or an empty Optional otherwise.
     */
    public Optional<Movie> findByName(final String name) {
        return movieRepository.findByName(name);
    }

    /**
     * Retrieves all movies of a given genre.
     *
     * @param genre The genre to filter the movies.
     * @return A collection of movies of the specified genre.
     */
    public Collection<Movie> findByGenre(final MovieGenre genre) {
        return movieRepository.findByGenre(genre);
    }

    /**
     * Retrieves all possible movie genres.
     *
     * @return A collection of all possible movie genres.
     */
    public Collection<MovieGenre> getPossibleGenres() {
        return movieRepository.getPossibleGenres();
    }

    /**
     * Adds a new movie to the repository.
     *
     * @param movie The movie to be added.
     */
    public void add(final Movie movie) {
        movieRepository.add(movie);
    }

    /**
     * Deletes a movie from the repository.
     *
     * @param movie The movie to be deleted.
     */
    public void delete(final Movie movie) {
        movieRepository.delete(movie);
    }

    /**
     * Updates a movie in the repository.
     *
     * @param movie The movie to be updated.
     */
    public void update(final Movie movie) {
        movieRepository.update(movie);
    }

}
