package cz.muni.fi.pa165.moviesmanagement.model;

import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;

import java.util.Collection;
import java.util.List;

/**
 * Movie entity change to jakarta persistence entity in M2
 */
public class Movie {
    private Long id;
    private String name;
    private String director;
    private String description;
    private List<String> actors;
    private Collection<MovieGenre> genres;
    private String imageUrl;

    /**
     * Constructor for Movie.
     *
     * @param id          The identifier of the movie.
     * @param name        The name of the movie.
     * @param director    The director of the movie.
     * @param description The description of the movie.
     * @param genres      The genres of the movie.
     * @param actors      The actors of the movie.
     * @param imageUrl    The URL of the image of the movie.
     */
    public Movie(final Long id, final String name, final String director, final String description,
                 final Collection<MovieGenre> genres, final List<String> actors, final String imageUrl) {
        this.id = id;
        this.name = name;
        this.director = director;
        this.description = description;
        this.genres = genres;
        this.actors = actors;
        this.imageUrl = imageUrl;
    }

    /**
     * Constructor for Movie.
     */
    public Movie() {
        // This constructor is intentionally empty.
    }

    /**
     * Gets the identifier of the movie.
     *
     * @return The identifier of the movie.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the identifier of the movie.
     *
     * @param id The identifier of the movie.
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the name of the movie.
     *
     * @return The name of the movie.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the movie.
     *
     * @param name The name of the movie.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the director of the movie.
     *
     * @return The director of the movie.
     */
    public String getDirector() {
        return director;
    }

    /**
     * Sets the director of the movie.
     *
     * @param director The director of the movie.
     */
    public void setDirector(final String director) {
        this.director = director;
    }

    /**
     * Gets the description of the movie.
     *
     * @return the description of the movie
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description of the movie.
     *
     * @param description the description of the movie
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the actors of the movie.
     *
     * @return The actors of the movie.
     */
    public List<String> getActors() {
        return actors;
    }

    /**
     * Sets the actors of the movie.
     *
     * @param actors The actors of the movie.
     */
    public void setActors(final List<String> actors) {
        this.actors = actors;
    }

    /**
     * Gets the genres of the movie.
     *
     * @return The genres of the movie.
     */
    public Collection<MovieGenre> getGenres() {
        return genres;
    }

    /**
     * Sets the genres of the movie.
     *
     * @param genres The genres of the movie.
     */
    public void setGenres(final Collection<MovieGenre> genres) {
        this.genres = genres;
    }

    /**
     * Gets the URL of the image of the movie.
     *
     * @return The URL of the image of the movie.
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Sets the URL of the image of the movie.
     *
     * @param imageUrl The URL of the image of the movie.
     */
    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
