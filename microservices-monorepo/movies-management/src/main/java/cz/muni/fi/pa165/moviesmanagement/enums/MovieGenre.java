package cz.muni.fi.pa165.moviesmanagement.enums;

public enum MovieGenre {
    ACTION,
    ADVENTURE,
    COMEDY,
    DRAMA,
    HORROR,
    ROMANCE,
    SCIFI,
    THRILLER,
    ANIMATION,
    DOCUMENTARY,
    FANTASY,
    MYSTERY,
    CRIME,
    WESTERN,
    HISTORICAL,
    MUSICAL,
    WAR,
    SPORT,
    BIOGRAPHY,
    MUSIC,
    PSYCHOLOGICAL,
    CRIMINAL
}
