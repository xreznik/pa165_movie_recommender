package cz.muni.fi.pa165.moviesmanagement.dto;


import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;

import java.util.Collection;
import java.util.List;

/**
 * Data Transfer Object for Movie.
 * This class encapsulates all information about a movie necessary for business logic and presentation layers.
 */
public class MovieDto {

    private Long id;
    @NotBlank(message = "Name cannot be empty")
    private String name;
    @NotBlank(message = "Director cannot be empty")
    private String director;
    @NotBlank(message = "Description cannot be empty")
    private String description;
    @NotEmpty(message = "At least one actor must be provided")
    private List<String> actors;
    @NotEmpty(message = "Genres cannot be empty")
    private Collection<MovieGenre> genres;
    private String imageUrl;

    /**
     * Gets the ID of the movie.
     *
     * @return the ID of the movie
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the ID of the movie.
     *
     * @param id the ID of the movie
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the name of the movie.
     *
     * @return the name of the movie
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the movie.
     *
     * @param name the name of the movie
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the director of the movie.
     *
     * @return the director of the movie
     */
    public String getDirector() {
        return director;
    }

    /**
     * Sets the director of the movie.
     *
     * @param director the director of the movie
     */
    public void setDirector(final String director) {
        this.director = director;
    }

    /**
     * Gets the description of the movie.
     *
     * @return the description of the movie
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description of the movie.
     *
     * @param description the description of the movie
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the actors of the movie.
     *
     * @return the actors of the movie
     */
    public List<String> getActors() {
        return actors;
    }

    /**
     * Sets the actors of the movie.
     *
     * @param actors the actors of the movie
     */
    public void setActors(final List<String> actors) {
        this.actors = actors;
    }

    /**
     * Gets the genres of the movie.
     *
     * @return the genres of the movie
     */
    public Collection<MovieGenre> getGenres() {
        return genres;
    }

    /**
     * Sets the genres of the movie.
     *
     * @param genres the genres of the movie
     */
    public void setGenres(final Collection<MovieGenre> genres) {
        this.genres = genres;
    }

    /**
     * Gets the image URL of the movie.
     *
     * @return the image URL of the movie
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Sets the image URL of the movie.
     *
     * @param imageUrl the image URL of the movie
     */
    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
