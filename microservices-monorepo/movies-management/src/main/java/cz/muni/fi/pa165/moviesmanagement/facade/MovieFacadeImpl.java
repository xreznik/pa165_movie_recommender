package cz.muni.fi.pa165.moviesmanagement.facade;

import cz.muni.fi.pa165.moviesmanagement.dto.CreateMovieDto;
import cz.muni.fi.pa165.moviesmanagement.dto.MovieDto;
import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.mapper.MovieMapper;
import cz.muni.fi.pa165.moviesmanagement.model.Movie;
import cz.muni.fi.pa165.moviesmanagement.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.Collection;
import java.util.Optional;

@Component
public class MovieFacadeImpl implements MovieFacade {

    private final MovieService movieService;
    private final MovieMapper movieMapper;

    /**
     * Constructor for MovieFacadeImpl.
     *
     * @param movieService The movie service.
     * @param movieMapper  The movie mapper.
     */
    @Autowired
    public MovieFacadeImpl(final MovieService movieService, final MovieMapper movieMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
    }

    /**
     * Retrieves all movies.
     *
     * @return A collection of all movie DTOs.
     */
    @Override
    public Collection<MovieDto> getAllMovies() {
        return movieService.getAll().stream()
                .map(movieMapper::convertToDto)
                .toList();
    }

    /**
     * Retrieves a movie by its identifier.
     *
     * @param id The identifier of the movie.
     * @return An Optional containing the movie DTO if found, or an empty Optional otherwise.
     */
    @Override
    public Optional<MovieDto> getMovieById(final Long id) {
        return movieService.findById(id)
                .map(movieMapper::convertToDto);
    }

    /**
     * Retrieves a movie by its name.
     *
     * @param name The name of the movie.
     * @return An Optional containing the movie DTO if found, or an empty Optional otherwise.
     */
    @Override
    public Optional<MovieDto> getMovieByName(final String name) {
        return movieService.findByName(name)
                .map(movieMapper::convertToDto);
    }

    /**
     * Retrieves all movies of a given genre.
     *
     * @param genre The genre to filter the movies.
     * @return A collection of movie DTOs with the specified genre.
     */
    @Override
    public Collection<MovieDto> getMoviesByGenre(final MovieGenre genre) {
        return movieService.findByGenre(genre).stream()
                .map(movieMapper::convertToDto)
                .toList();
    }

    /**
     * Retrieves all possible movie genres.
     *
     * @return A collection of all possible movie genres that are accepted as values elsewhere.
     */
    @Override
    public Collection<MovieGenre> getPossibleGenres() {
        return movieService.getPossibleGenres();
    }

    /**
     * Adds a new movie to the repository.
     *
     * @param movieDto The movie DTO to add.
     * @return The added movie DTO.
     */
    @Override
    public MovieDto addMovie(final CreateMovieDto movieDto) {
        Movie movie = movieMapper.convertToEntity(movieDto);
        movieService.add(movie);
        return movieMapper.convertToDto(movie);
    }

    /**
     * Updates a movie in the repository.
     *
     * @param movieDto The movie DTO to update.
     * @return The updated movie DTO.
     */
    @Override
    public MovieDto updateMovie(final MovieDto movieDto) {
        Movie movie = movieMapper.convertToEntity(movieDto);
        movieService.update(movie);
        return movieMapper.convertToDto(movie);
    }

    /**
     * Deletes a movie by its identifier.
     *
     * @param id The identifier of the movie to delete.
     * @return true if the movie was successfully deleted, false otherwise.
     */
    @Override
    public Boolean deleteMovie(final Long id) {
        Optional<Movie> movie = movieService.findById(id);
        if (movie.isEmpty()) {
            return false;
        }
        movie.ifPresent(movieService::delete);
        return true;
    }
}
