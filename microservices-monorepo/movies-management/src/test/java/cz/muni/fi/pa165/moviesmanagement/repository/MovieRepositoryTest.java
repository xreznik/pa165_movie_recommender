package cz.muni.fi.pa165.moviesmanagement.repository;

import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.model.Movie;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MovieRepositoryTest {

    private MovieRepository movieRepository;

    @Mock
    private Movie mockedMovie;

    @BeforeEach
    void setUp() {
        movieRepository = new MovieRepository();
    }

    @Test
    void addMovie_allCorrect() {
        movieRepository.add(mockedMovie);
        assertEquals(1, movieRepository.getAll().size());
        assertTrue(movieRepository.getAll().contains(mockedMovie));
    }

    @Test
    void deleteMovie_allCorrect() {
        movieRepository.add(mockedMovie);
        movieRepository.delete(mockedMovie);
        assertTrue(movieRepository.getAll().isEmpty());
    }

    @Test
    void updateMovie_allCorrect() {
        movieRepository.add(mockedMovie);
        Movie updatedMovie = new Movie();
        updatedMovie.setId(mockedMovie.getId());
        movieRepository.update(updatedMovie);
        assertEquals(updatedMovie, movieRepository.findById(mockedMovie.getId()).get());
    }

    @Test
    void findById_allCorrect_returnsMovie() {
        movieRepository.add(mockedMovie);
        Optional<Movie> foundMovie = movieRepository.findById(mockedMovie.getId());
        assertTrue(foundMovie.isPresent());
        assertEquals(mockedMovie, foundMovie.get());
    }

    @Test
    void  findById_notFoundId_returnsEmpty() {
        Optional<Movie> foundMovie = movieRepository.findById(999L);
        assertTrue(foundMovie.isEmpty());
    }

    @Test
    void findByName_allCorrect_returnsMovie() {
        when(mockedMovie.getName()).thenReturn("Test Movie");
        movieRepository.add(mockedMovie);
        Optional<Movie> foundMovie = movieRepository.findByName("Test Movie");
        assertTrue(foundMovie.isPresent());
        assertEquals(mockedMovie, foundMovie.get());
    }

    @Test
    void findByName_notFound_returnsEmpty() {
        Optional<Movie> foundMovie = movieRepository.findByName("Non-existent Movie");
        assertTrue(foundMovie.isEmpty());
    }

    @Test
    void findByGenre_allCorrect_returnsMovieCollection() {
        Movie movie1 = new Movie();
        movie1.setGenres(Collections.singleton(MovieGenre.ACTION));
        Movie movie2 = new Movie();
        movie2.setGenres(Collections.singleton(MovieGenre.COMEDY));
        movieRepository.add(movie1);
        movieRepository.add(movie2);
        Collection<Movie> foundMovies = movieRepository.findByGenre(MovieGenre.ACTION);
        assertEquals(1, foundMovies.size());
        assertTrue(foundMovies.contains(movie1));
    }

    @Test
    void getPossibleGenres_allCorrect_returnsGenresCollections() {
        assertEquals(MovieGenre.values().length, movieRepository.getPossibleGenres().size());
    }
}

