package cz.muni.fi.pa165.moviesmanagement.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.moviesmanagement.dto.CreateMovieDto;
import cz.muni.fi.pa165.moviesmanagement.dto.MovieDto;
import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.facade.MovieFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith({SpringExtension.class, MockitoExtension.class})
@WebMvcTest(MovieController.class)
@AutoConfigureMockMvc
public class MovieControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MovieFacade movieFacade;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void getAllMovies_allCorrect_returnsMovieDtoCollection() throws Exception {
        MovieDto movie = new MovieDto();
        movie.setId(1L);
        movie.setName("Test Movie");
        when(movieFacade.getAllMovies()).thenReturn(Collections.singletonList(movie));

        mockMvc.perform(get("/api/movies"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").value("Test Movie"));
    }

    @Test
    public void getMovieById_allCorrect_returnsMovieDtoCollection() throws Exception {
        MovieDto movie = new MovieDto();
        movie.setId(1L);
        movie.setName("Test Movie");
        when(movieFacade.getMovieById(1L)).thenReturn(Optional.of(movie));

        mockMvc.perform(get("/api/movies/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("Test Movie"));
    }

    @Test
    public void getMovieByName_allCorrect_returnsMovieDto() throws Exception {
        MovieDto movie = new MovieDto();
        movie.setId(1L);
        movie.setName("TestMovie");
        when(movieFacade.getMovieByName("TestMovie")).thenReturn(Optional.of(movie));

        mockMvc.perform(get("/api/movies/name/TestMovie"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("TestMovie"));
    }

    @Test
    public void getMoviesByGenre_allCorrect_returnsMovieDtoCollection() throws Exception {
        MovieDto movie = new MovieDto();
        movie.setId(1L);
        movie.setName("Test Movie");
        when(movieFacade.getMoviesByGenre(ArgumentMatchers.any(MovieGenre.class)))
                .thenReturn(Collections.singletonList(movie));

        mockMvc.perform(get("/api/movies/genre/COMEDY"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").value("Test Movie"));
    }

    @Test
    public void getGenres_allCorrect_returnsGenresValues() throws Exception {
        when(movieFacade.getPossibleGenres()).thenReturn(Arrays.asList(MovieGenre.values()));

        mockMvc.perform(get("/api/movies/genres"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    public void addMovie_allCorrect_returnsMovieDto() throws Exception {
        CreateMovieDto createMovieDto = new CreateMovieDto(
                "Test Movie",
                "Test Director",
                "Test Description",
                Collections.singletonList(MovieGenre.ACTION),
                "testImageUrl",
                Collections.singletonList("Test Actor")
        );

        MovieDto movie = new MovieDto();
        movie.setId(1L);
        movie.setName("Test Movie");
        movie.setGenres(Collections.singleton(MovieGenre.COMEDY));
        when(movieFacade.addMovie(createMovieDto)).thenReturn(movie);

        mockMvc.perform(post("/api/movies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(createMovieDto)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("Test Movie"));
    }

    @Test
    public void deleteMovie_allCorrect_returnsTrue() throws Exception {
        when(movieFacade.deleteMovie(1L)).thenReturn(true);

        mockMvc.perform(delete("/api/movies/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").value(true));
    }
}
