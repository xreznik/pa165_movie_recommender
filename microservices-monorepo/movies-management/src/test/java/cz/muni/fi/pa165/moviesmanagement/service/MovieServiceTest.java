package cz.muni.fi.pa165.moviesmanagement.service;

import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.model.Movie;
import cz.muni.fi.pa165.moviesmanagement.repository.MovieRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MovieServiceTest {

    @Mock
    private MovieRepository movieRepository;

    @InjectMocks
    private MovieService movieService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAllMovies_allCorrect_returnsMoviesCollection() {
        // Arrange
        Collection<Movie> movies = Arrays.asList(
                new Movie(),
                new Movie()
        );
        when(movieRepository.getAll()).thenReturn(movies);

        // Act
        Collection<Movie> result = movieService.getAll();

        // Assert
        assertEquals(movies.size(), result.size());
        assertTrue(result.containsAll(movies));
    }

    @Test
    void findById_allCorrect_returnsMovie() {
        // Arrange
        Long id = 1L;
        Movie movie = new Movie();
        when(movieRepository.findById(id)).thenReturn(Optional.of(movie));

        // Act
        Optional<Movie> result = movieService.findById(id);

        // Assert
        assertTrue(result.isPresent());
        assertEquals(movie, result.get());
    }

    @Test
    void findById_notFoundId_returnsEmpty() {
        // Arrange
        Long id = 999L;
        when(movieRepository.findById(id)).thenReturn(Optional.empty());

        // Act
        Optional<Movie> result = movieService.findById(id);

        // Assert
        assertTrue(result.isEmpty());
    }

    @Test
    void findByGenre_allCorrect_returnsMovies() {
        // Arrange
        MovieGenre genre = MovieGenre.ACTION;
        Collection<Movie> movies = Arrays.asList(
                new Movie(),
                new Movie()
        );
        when(movieRepository.findByGenre(genre)).thenReturn(movies);

        // Act
        Collection<Movie> result = movieService.findByGenre(genre);

        // Assert
        assertEquals(movies.size(), result.size());
        assertTrue(result.containsAll(movies));
    }

    @Test
    void getPossibleGenres_allCorrect_returnsGenresCollection() {
        // Arrange
        Collection<MovieGenre> genres = Arrays.asList(MovieGenre.ACTION, MovieGenre.DRAMA);
        when(movieRepository.getPossibleGenres()).thenReturn(genres);

        // Act
        Collection<MovieGenre> result = movieService.getPossibleGenres();

        // Assert
        assertEquals(genres.size(), result.size());
        assertTrue(result.containsAll(genres));
    }

    @Test
    void addMovie_allCorrect_returnsMovie() {
        // Arrange
        Movie movie = new Movie();

        // Act
        movieService.add(movie);

        // Assert
        verify(movieRepository, times(1)).add(movie);
    }

    @Test
    void deleteMovie_allCorrect() {
        // Arrange
        Movie movie = new Movie();

        // Act
        movieService.delete(movie);

        // Assert
        verify(movieRepository, times(1)).delete(movie);
    }

    @Test
    void updateMovie_allCorrect() {
        // Arrange
        Movie movie = new Movie();

        // Act
        movieService.update(movie);

        // Assert
        verify(movieRepository, times(1)).update(movie);
    }
}
