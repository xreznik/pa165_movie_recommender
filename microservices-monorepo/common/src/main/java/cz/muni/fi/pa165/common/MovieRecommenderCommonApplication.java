package cz.muni.fi.pa165.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SuppressWarnings({"PMD", "checkstyle:hideutilityclassconstructor"})  // spring triggers linters false-positives
@SpringBootApplication
public class MovieRecommenderCommonApplication {

	/**
	 * Main method to run the application.
	 * @param args command line arguments
	 */
	public static void main(final String[] args) {
		SpringApplication.run(MovieRecommenderCommonApplication.class, args);
	}

}
