# PA165_movie_recommender

## Project Description
The web application is a catalogue of movies of different genres. Each movie has a title, description, film director, and list of actors, and image(s) from the main playbill. Each movie is categorized in one or more genres. Only the administrator can create, remove and update the list of movies. Users can rate the movies according to different criteria (e.g how novel are the ideas of the movie, their final score, etc…). The main feature of the system is that users can pick one movie and get the list of similar movies and / or movies that were liked the most by other users watching the same movie (no need of complex algorithms, some simple recommendation is enough!).


## Microservices:
#### 1. Movies Management Service
Handles CRUD operations for movies, including title, description, film director, actors, images, and genres. This service is used by the administrator for managing the movie catalogue.
#### 2. User Management and Authentication Service
Manages user accounts, authentication, and authorization. This ensures that only administrators have the ability to modify the movie catalogue, while users can rate movies and view recommendations.
#### 3. Ratings and Reviews Service
Handles the storage, retrieval, and processing of movie ratings and reviews according to different criteria. This service aggregates user ratings to provide insights and data for recommendations.
#### 4. Recommendation Service
Provides movie recommendations based on simple algorithms, such as movies with similar genres or high ratings from users who liked the same movies. This service can use the data from the Ratings and Reviews Service to generate recommendations.


## Use case diagram
![Use case Diagram](docs/diagrams/useCase-diagram.png)
## Class diagram for DTO's
![DTO Class Diagram](docs/diagrams/dto-class-diagram.png)

